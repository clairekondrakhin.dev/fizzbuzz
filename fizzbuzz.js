console.log("fizzBuzz");
let i = 0; //ici je définis ma variable i
for (
  let i = 0;
  i <= 100;
  i++ //ici je définis ma boucle si i est égale à zero et inférieur ou égal à 100 alors on ajoute 1 à i. La boucle s'arrete donc quand i est égal à 100
) {
  if (i % 3 === 0 && i % 5 === 0) {
    console.log("fizzbuzz");
    // ici je définis la 1ère condition : si i est multiple de 3 et 5 alors j'affiche fizzbuzz à la place du nombre
  } else {
    if (i % 5 === 0) {
      console.log("buzz");
      // ici je définis ma 2ème condition: si i est uniquement multiple de 5 alors j'affiche buzz à la place du nombre
    } else {
      if (i % 3 === 0) {
        console.log("fizz");
        // ici je définis ma 3ème condition: si i est uniquement multiple de 3 alors j'affiche fizz à la place du nombre
      } else {
        console.log(i);
        // si aucune des 3 conditions n'est remplie alors le nombre s'affiche
      }
    }
  }
}
